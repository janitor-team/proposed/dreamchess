dreamchess (0.3.0-2) unstable; urgency=medium

  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.6.2.

 -- Markus Koschany <apo@debian.org>  Fri, 24 Feb 2023 00:42:37 +0100

dreamchess (0.3.0-1) unstable; urgency=medium

  * New upstream version 0.3.0.
  * Switch to debhelper-compat = 12.
  * Declare compliance with Debian Policy 4.4.0.
  * Remove --no-parallel option.
  * Remove clean file.
  * Use override for dh_missing.
  * Drop all patches. Fixed upstream.

 -- Markus Koschany <apo@debian.org>  Mon, 22 Jul 2019 01:10:11 +0200

dreamchess (0.2.1-RC2-3) unstable; urgency=medium

  * Switch to compat level 11.
  * Declare compliance with Debian Policy 4.2.1.
  * Drop deprecated menu file and xpm icon.
  * Use https for Homepage field.
  * Remove get-orig-source target.
  * Use canonical VCS URI.

 -- Markus Koschany <apo@debian.org>  Sat, 08 Dec 2018 15:31:49 +0100

dreamchess (0.2.1-RC2-2) unstable; urgency=medium

  * Build without --parallel option to avoid a race condition on buildd.

 -- Markus Koschany <apo@debian.org>  Tue, 10 Nov 2015 14:28:08 +0100

dreamchess (0.2.1-RC2-1) unstable; urgency=medium

  * Add myself to Uploaders.
  * Move the package to Git.
  * Imported Upstream version 0.2.1-RC2.
    - According to the NEWS file:
      Fixed issue with chess piece selection on some AMD GPUs.
      (Closes: #744237)
    - The chess board will be turned now when the user is playing black against
      the computer. (Closes: #578000)
    - The game works well and is not slow. (Closes: #606232)
    - Changing the resolution works with the free radeon driver.
      (Closes: #620103)
  * Declare compliance with Debian Policy 3.9.6.
  * Use compat level 9 and require debhelper >= 9.
  * d/rules: Remove --with autotools_dev because --with autoreconf
    is a superset.
  * Update debian/watch and track releases at github.com.
  * Add get-orig-source target.
  * Add bison and flex to Build-Depends.
  * debian/rules: Fix path to dreamchess.png
  * debian/rules: Do not pass buildflags to dh_auto_configure manually.
  * Add git_rev.patch and fix FTBFS.
  * Add desktop-file.patch and use upstream's desktop file from now on.
  * wrap-and-sort -sa.
  * Update debian/copyright to copyright format 1.0.
  * Add libgl1-mesa-dev, libglew-dev and libglu1-mesa-dev to Build-Depends
    otherwise the game will not start.
  * Install new dreamchess.png file to hicolor icon directory.
  * Update the clean file and ensure dreamchess can be built twice in a row.

 -- Markus Koschany <apo@debian.org>  Mon, 09 Nov 2015 19:44:59 +0100

dreamchess (0.2.0-3) unstable; urgency=low

  * Team upload.
  * Minor watch file fixes
  * Wrap build-deps, deps one per line
  * Wrap Debian menu file, one attribute per line
  * Switch to dpkg-source v3 quilt
  * Fix FTBFS with GCC 4.5 (Closes: #554498)
  * Switch to debhelper 7 and dh
  * Use dh-autoreconf instead of patching autotools stuff
  * Add ${misc:Depends} to -data package depends just in case
  * Bump Standards-Version, no changes needed
  * Forward the patches upstream and add DEP-3 headers
  * Enable audio using SDL mixer
  * Improve the package descriptions
  * Drop the pthread patch since it is no longer needed
  * Create the XPM icon from the upstream icon using imagemagick

 -- Paul Wise <pabs@debian.org>  Fri, 25 Mar 2011 12:54:56 +0800

dreamchess (0.2.0-2) unstable; urgency=low

  [ Cyril Brulebois ]
  * Remove “-p1” from debian/patches/series since it's useless and since
    it might cause an FTBFS with the “3.0 (quilt)” source package format.
  * Move config.{guess,sub} update from the clean target to the
    config.status one, and remove them in clean target instead, so as to
    keep the Debian diff clean and not to cause an FTBFS with the new
    “3.0 (quilt)” source package format. Thanks to Raphaël Hertzog for the
    notice (Closes: #484970).
  * debian/rules cleanup:
     - Nuke trailing spaces.
     - Use quilt.make include, and appropriate patch/unpatch dependencies
       for the config.status and clean target, using an additional
       realclean target for the latter.

  [ Gonéri Le Bouder ]
  * Use the Games/Board section for the Debian menu file

  [ Barry deFreese ]
  * Update my e-mail address.
  * Change short description on -data package.
  * Add description to quilt patch.
  * Bump Standards Version to 3.8.1. (No changes needed).

 -- Barry deFreese <bdefreese@debian.org>  Wed, 18 Mar 2009 14:17:56 -0400

dreamchess (0.2.0-1) unstable; urgency=low

  [ Barry deFreese ]
  * New upstream release (Closes: #457050)
  * Drop mxml patch as upstream no longer ships it

  [ Gonéri Le Bouder ]
  * add fix-mxml-check.diff, -lpthread was missing in ./configure libmxml
    detection
  * dreamchess depends on dreamchess-data (= ${source:Version})
  * dreamchess-data suggets dreamchess
  * remove dreamchess-fullscreen.desktop since it's now possible to switch
    to the fullscreen from within the game
  * update the copyright file. The game is now under the GPLv3.

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Sat, 16 Feb 2008 03:55:09 +0100

dreamchess (0.1.0-3) unstable; urgency=low

  [ Gonéri Le Bouder ]
  * dpkg-buildpackage -B doesn't create arch all package anymore
    (Closes: #443046)
  * change the name of the menu entry for the full screen desktop file
    to make them distinct in the application menus
    (Closes: #454936)

  [ Jon Dowland ]
  * add Homepage: control field to source stanza

  [ Barry deFreese ]
  * Remove Homepage from package description
  * Update VCS fields in control
  * Bump to standards version 3.7.3 (No changes necessary)
  * Remove deprecated encoding line from desktop file
  * Add myself to uploaders

 -- Barry deFreese <bddebian@comcast.net>  Tue, 18 Dec 2007 07:35:14 -0500

dreamchess (0.1.0-2) unstable; urgency=low

  * fix the watchfile
  * rewrite use_mxml_from_debian.diff to avoid an autotools autoexecution
    (Closes: #424186, #442544)
  * make clean removes quilt .pc directory
  * do not ignore make distclean return anymore

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Sun, 16 Sep 2007 22:28:15 +0200

dreamchess (0.1.0-1) unstable; urgency=low

  * initial package (Closes: #403123)

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Thu, 14 Dec 2006 20:02:36 +0100

